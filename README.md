# Lab Automation Methods

This GitLab project is dedicated to the exchange of methods for discrete process management systems (PMS) used in laboratory automation projects. An example of such an PMS is the Thermo Fisher Scientific Momentum, short TFS Momentum. 

Please bear in mind that all material contained in here (methods, experiments, instrumentations, ...) is publicly available underlying the Apache 2.0 license.